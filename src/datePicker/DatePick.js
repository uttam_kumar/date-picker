import React, { Component } from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

class DatePick extends Component {
  constructor(props) {
    super(props);
    console.log(this.props)
    this.state = {
      date: new Date(),
      validationError : ""
    };
  }

  componentDidMount() {
    this.initDateTimePicker()  // date time ui initializer



    let customDate = this.props.fieldValue ? new Date(this.props.fieldValue.split(" ")[0]) : new Date()
    console.log(customDate)
    this.setState(
      {
        date: customDate
      },
      () => this.sendToPayload(this.formattedDate(customDate))
    );

  }

  showHideErrorMsg = (v) => {
    if(v.status === "invalid"){
      this.setState({validationError : v.msg})
    }else {
      this.setState({validationError : ""})
    }
  }



  insertAfter = (newNode, referenceNode) => {
    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
  };

  initDateTimePicker = () => {
    this.dateContainer
      .querySelector(".react-datepicker__input-container")
      .classList.add("mdc-text-field");


    //create a label for date input
    let dateInputContainer = this.dateContainer.querySelector(
      ".react-datepicker__input-container input"
    );
    var createDateLabel = document.createElement("label");
    createDateLabel.classList.add(
      "mdc-floating-label",
      "custom-datepicker-floating-label"
    );
    createDateLabel.setAttribute("for", "my-text-field");
    createDateLabel.innerHTML = "date";
    this.insertAfter(createDateLabel, dateInputContainer);

  }

  formattedDate = date => {
    
    return `${date.getUTCFullYear()}-${(
      "0" +
      (date.getUTCMonth() + 1)
    ).slice(-2)}-${("0" + date.getUTCDate()).slice(-2)}`;
  };


  sendToPayload = (formattedDate) => {
    console.log(formattedDate)
   
  };

  generateDate = newDate => {
    this.setState({
      date: newDate
    }, () => this.sendToPayload(this.formattedDate(newDate)));
  };


  render() {

    const Input = ({ onChange, placeholder, value, isSecure, id, onClick }) => (
      <React.Fragment>
        <input
          onChange={onChange}
          placeholder={placeholder}
          value={value}
          isSecure={isSecure}
          id={id}
          onClick={onClick}
          type={"text"}
          className="mdc-text-field__input"
          readonly="false"
        />
      </React.Fragment>
    );
    return (
      <div className="mdc-layout-grid custom-dialog-layout-grid custom-datepicker-wrapper">
        <div className="mdc-layout-grid__inner">
          <div className="mdc-layout-grid__cell--span-4 ">
            <span >{this.props.label}</span>
          </div>
          <div
            className="mdc-layout-grid__cell--span-8 "
            ref={container => {
              this.dateContainer = container;
            }}
          >
            <DatePicker
              selected={this.state.date}
              onChange={this.generateDate}
              customInput={<Input />}
            />
          </div>
        </div>
        <span className="text-error-message">{this.state.validationError}</span>

      </div>
    );
  }
}

export default DatePick;