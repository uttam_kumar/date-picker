import React, { Component } from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { tr } from "date-fns/esm/locale";

class PickDate extends Component {
  render() {
    return (
      <div>
        <br />

        {this.props.labelRequired && <span>{this.props.label + " : "}</span>}

        <DatePicker
          //diaplay only calender
          inline={this.props.inline || false}
          //calender height fixed for every month
          fixedHeight={this.props.fixedHeight || false}
          // displaying week number
          showWeekNumbers={this.props.showWeekNumbers || false}
          // date format in textbox
          dateFormat={this.props.dateFormat || "dd/MM/yyyy"}
          // maximum date selected by datepicker
          maxDate={this.props.maxDate || null}
          // minimum date selected by datepicker
          // minDate={this.props.minDate || null}

          // selected date
          selected={this.props.selectedDate || new Date()}
          // changing date
          onChange={this.props.onChange || null}
          // enable or disable today button, if props true then set "Today"
          todayButton={this.props.todayButton ? "Today" : ""}
          // show time for selecting
          // showTimeSelect={this.props.showTimeSelect || false}

          // show month in drop down
          // showMonthDropdown={this.props.showMonthDropdown || false}

          // show short month in drop down
          // useShortMonthInDropdown={this.props.useShortMonthInDropdown || false}

          // show year in drop down
          showYearDropdown={this.props.showYearDropdown || false}
          // show drop down year format
          dateFormatCalendar={this.props.dateFormatCalendar || "MMMM"}
          // year shows on drop down at a time
          yearDropdownItemNumber={this.props.yearDropdownItemNumber || 25}
          // scrollable drop down item
          scrollableYearDropdown={this.props.scrollableYearDropdown || false}
          // place holder of text box
          placeholderText={this.props.placeholderText || "MM/DD/YYYY"}
        />
      </div>
    );
  }
}

export default PickDate;
