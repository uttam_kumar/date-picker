import logo from "./logo.svg";
import "./App.css";
import React, { Component } from "react";
import PickDate from "./datePicker/PickDate";

class App extends Component {
  state = {
    startDate: new Date()
  };

  //validation handled from here
  handleChange = date => {
    //console.log(date);
    this.setState({
      startDate: date
    });
  };

  currentselectedDate = () => {
    console.log(this.state.startDate);
  };

  customDate = year => {
    var cDate = new Date();
    cDate.setFullYear(cDate.getFullYear() - year);
    console.log(cDate);
    return cDate;
  };

  render() {
    return (
      <div className="App">
        <PickDate
          todayButton={true}
          fixedHeight={true}
          showWeekNumbers={true}
          // changing date
          onChange={this.handleChange}
          // showTimeSelect={false}
          // dateFormat="yyyy/MM/dd"
          selectedDate={this.state.startDate}
          //maxDate={this.customDate(5)}
          maxDate={new Date()}
          //  minDate={new Date()}
          labelRequired={true}
          label="Date"
          // showMonthDropdown={true}
          // useShortMonthInDropdown={true}
          showYearDropdown={true}
          // dateFormatCalendar="MMMM"
          yearDropdownItemNumber={50}
          scrollableYearDropdown={true}
        />

        <button onClick={this.currentselectedDate}>check time</button>

        
      </div>
    );
  }
}

export default App;
